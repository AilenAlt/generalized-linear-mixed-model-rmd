# Análisis de datos: abundancia de parasitos en Misiones, Argentina
## Trabajo final de la materia Biometria II (bioestadística)

El objetivo principal de este trabajo consistió en el analisis la comunidad endoparasitaria de nematodes en *Rattus rattus* que habita áreas urbanas, periurbanas y rurales del departamento de Iguazú, Misiones.